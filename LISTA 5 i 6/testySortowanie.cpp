#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include <cmath>



using namespace std;

void wstawianie(int tab[], int dl);
int partycjonowanie( int tab[], int lewy, int prawy);
void kopiec(int tablica[], int pocz);
void introspektywne(int tab[], int pocz, int koniec);
void scalanie(int pom[],int tab[], int pocz, int koniec);
void Heapify(int tab[], int poczatek, int koniec);
void quicksort(int tab[], int poczatek, int koniec);
int podzielTabliceOdwr(int tablica[], int pocz, int koniec);
void quicksortOdwr(int tablica[], int pocz, int koniec);
void wstawianie(int tab[], int dl);


int partycjonowanie( int tab[], int lewy, int prawy)
{
    int i = lewy;
    int j = prawy;
    int x = tab[(lewy + prawy)/2];
    do
    {
        while(tab[i]<x)
             i++;
        while(tab[j]>x)
             j--;

        if(i<=j)
        {
            swap(tab[i],tab[j]);
            i++;
            j--;
        }
    } while(i<=j);
    return j;
}

void kopiec(int tablica[], int pocz)
{
  int i;
  for (i=(pocz-2)/2; i>=0; --i)
    Heapify(tablica, i, pocz);
  for (i=pocz-1; i>0; --i)
  {
    swap(tablica[0],tablica[i]);
    Heapify(tablica, 0, i);
  }
}

void introspektywne(int tab[], int pocz, int koniec)
{
    if(koniec<2)
        return;

    if(koniec<9)
    {
        wstawianie(tab, koniec);
        return;
    }

    if (pocz>(int)floor(2*log(koniec)/M_LN2))
    {
        kopiec(tab, koniec);
        return;
    }

    int p=partycjonowanie(tab, 0, koniec);
    introspektywne(tab, pocz+1, p+1);
    introspektywne(&tab[p+1], pocz+1, koniec-p-1);
}

void scalanie(int pom[],int tab[], int pocz, int koniec)
{
    int i;
    int srodek;
    int i1,i2;

    srodek = (pocz + koniec + 1) / 2;

    if(srodek - pocz > 1)
        scalanie(pom, tab, pocz, srodek - 1);

    if(koniec - srodek > 0)
        scalanie(pom, tab, srodek, koniec);

    i1 = pocz;
    i2 = srodek;
    int temp=tab[i1];

    for(i=pocz; i<=koniec; i++)
        pom[i] = ((i1 == srodek) || ((i2 <= koniec) && (tab[i1] > tab[i2]))) ? tab[i2++] : tab[i1++];

    for(i=pocz; i<=koniec; i++) tab[i] = pom[i];
}

void Heapify(int tab[], int poczatek, int koniec)
{
    int lewy = 2*poczatek+1;
    int prawy = 2*poczatek+2;
    int x = lewy;
    while ((lewy < koniec) && (tab[poczatek] < tab[x]))
    {
        lewy=2*poczatek+1;
        x=lewy;
        if (prawy<koniec && tab[prawy] > tab[lewy])
            x=prawy;
        swap(tab[poczatek],tab[x]);
        poczatek = x;
    }
}

void quicksort(int tab[], int poczatek, int koniec)
{
    if(poczatek<koniec)
    {
        int pivot=tab[(poczatek+koniec)/2];
        int i,j,x;
        i=poczatek;
        j=koniec;
    do
    {
        while(tab[i]<pivot)
            i++;
        while(tab[j]>pivot)
            j--;

        if(i<=j)
        {
            x=tab[i];
            tab[i]=tab[j];
            tab[j]=x;
            i++;
            j--;
        }
    }while(i<=j);

    if(j>poczatek) quicksort(tab, poczatek, j);
    if(i<koniec) quicksort(tab, i, koniec);
    }
}

int podzielTabliceOdwr(int tablica[], int pocz, int koniec)
{
    int os = tablica[pocz];
    int i = pocz;
    int j = koniec;
    int temp;

    while(true)
    {
        while(tablica[j]<os)
            j--;
        while(tablica[i]>os)
            i++;

        if(i<j)
        {
            temp = tablica[i];
            tablica[i] = tablica[j];
            tablica[j] = temp;
            i++;
            j--;
        }
        else
            return j;
    }
}
void quicksortOdwr(int tablica[], int pocz, int koniec)
{
    if (pocz<koniec)
    {
        int q = podzielTabliceOdwr(tablica, pocz, koniec);
        quicksortOdwr(tablica, pocz, q);
        quicksortOdwr(tablica, q+1, koniec);
    }
}

void wstawianie(int tab[], int dl)
{
    for(int i=1; i<dl; ++i)
        for(int j=i; j>0 && tab[j-1]>tab[j]; --j)
            swap(tab[j],tab[j-1]);
}



int main()
{
    LARGE_INTEGER frequency;        // ticks per second
    LARGE_INTEGER t1, t2;           // ticks
    double elapsedTime;
    srand(time(NULL));
    QueryPerformanceFrequency(&frequency);

    int rozmiar = 100;
    int * pom = new int[rozmiar];
    int * randTab1[100];

    for(int j=0;j<=99;j++)
    {
        randTab1[j] = new int[rozmiar];

        for(int i=0; i<rozmiar; i++)
            randTab1[j][i] = rand() % rozmiar;

           // quicksort(randTab1[j], 0, 0.25*rozmiar);

      //  for(int i=0;i<rozmiar;i++)
        //    cout << " " << randTab1[j][i];

        cout << endl;
    }

    cout << "Po" << endl;

    QueryPerformanceCounter(&t1);
    for(int j=0;j<1;j++)
    {
       // introspektywne(randTab1[j],0,rozmiar);
	//	quicksort(randTab1[j], 0, rozmiar-1);
	//	scalanie(pom,randTab1[j],0,rozmiar);
		
        for(int i=0;i<rozmiar;i++)
            cout << " " << randTab1[j][i];

        cout << endl;
    }
    QueryPerformanceCounter(&t2);

    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << "Czas dla(" << rozmiar << ")" << elapsedTime << " ms.\n";



    for(int i=0;i<100;i++)
        delete [] randTab1[i];


    cin.get();
    return 0;
}

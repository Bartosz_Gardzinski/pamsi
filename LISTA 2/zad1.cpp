#include <iostream>
#include <algorithm>
#include <ctime>
using namespace std;

string *palList;
int k=0;
/*////////////////////////////////////////////////////////////////////////////*/
bool jestPal(const string &testStr, int l,int z)
{
	if (l >= 2)
		{
		if (testStr[z] == testStr[l-1])
			{
			z++; l--;
			return jestPal(testStr,l,z);
			}
		else
			{
			
			return false;
			}
		}
	
	return true;		
}
/*----------------------------------------------------------------------------*/
void Permutacje(string slowo,int x)
{
	if(x < slowo.length()-1)
        {
 	    for(int i = x; i<slowo.length(); i++)
	  	    {
	  	    swap (slowo[x],slowo[i]);
	  	    Permutacje(slowo,x+1);
	  	    swap (slowo[x],slowo[i]);
  	        }
		}
		
		else
		{
			
			if(jestPal(slowo,slowo.length(),0))
    {
     palList[k]=slowo;
     k++;
    }
		}
		
		
}

int Silnia(int x)
{
	if(x<1) 
    return 1;

return x*Silnia(x-1);
}

void UsunDuplikat()
{
 for(int i=0;i<k-1;i++)
    for(int j=i+1;j<k; j++)
        if(palList[i] == palList[j])
        {
        for(int g=j;g<k-1;g++)
            palList[g] = palList[g+1];
            k--;
            j--;
        }
}
/*////////////////////////////////////////////////////////////////////////////*/
int main()
{
	string x;
	cout << "Podaj slowo ktore chcesz sprawdzic" << endl;
	cin >> x;
	
	const string& b = x;
	palList = new string[Silnia(x.length())];
	
	clock_t start = clock();
	Permutacje(b, 0);
	UsunDuplikat();
	cout <<"Czas wykonywania: "<< (clock() - start) <<endl;
	
	
	for(int i=0;i<k;i++)
    {
     cout<< palList[i] << " ";
     cout<< endl;
    }
delete[] palList;
}

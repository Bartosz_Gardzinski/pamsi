#include <iostream>
#include <cstdlib>
#include <algorithm>




using namespace std;
template <class type> 
class elementDrzewa;

template <class type> 
class element
{
    public:
    element<type>* nastepny;
    elementDrzewa<type>* syn;
    element<type>();
};
template <class type> 
element<type>::element()
{
    nastepny = 0;
    syn = 0;
}
template <class type> 
class Lista
{
    public:
    element<type> *pierwszy;
    void dodajElem(elementDrzewa<type>* synek);
    void usunElem(elementDrzewa<type>* kasuj);
    elementDrzewa<type>* zwrocElement(int nr);
    int rozmiar();
    Lista();
};
template <class type> 
Lista<type>::Lista()
{
    pierwszy = 0;
}
template <class type> 
elementDrzewa<type>* Lista<type>::zwrocElement(int nr)
{
    element<type>* temp = pierwszy;
    if(nr > rozmiar())
        cout << "Nieprawidlowy indeks!" << endl;
    else
        for(int i=1;i<nr;i++)
            temp = temp->nastepny;

    return temp->syn;
}
template <class type> 
void Lista<type>::dodajElem(elementDrzewa<type>* synek)
{
    element<type> *nowy = new element<type>();
    nowy->syn = synek;

    if(pierwszy == 0)
    {
        pierwszy = nowy;
    }
    else
    {
        element<type> *temp = pierwszy;

        while(temp->nastepny)
            temp = temp->nastepny;

        temp->nastepny = nowy;
        nowy->nastepny = 0;
    }
}
template <class type> 
void Lista<type>::usunElem(elementDrzewa<type>* kasuj)
{
    if(kasuj == pierwszy->syn)
    {
        element<type> *temp = pierwszy;
        pierwszy = temp->nastepny;
    }

    else
    {
        element<type> *temp = pierwszy;

        while(temp->nastepny->syn != kasuj)
        {
            temp = temp->nastepny;
        }

        if(temp->nastepny->nastepny == 0)
            temp->nastepny = 0;
        else
            temp->nastepny = temp->nastepny->nastepny;

        delete temp;
    }

}
template <class type> 
int Lista<type>::rozmiar()
{
    int licznik = 0;
    element<type> * temp = pierwszy;
    while(temp != 0)
    {
        temp = temp->nastepny;
        licznik++;
    }
    return licznik;
}

template <class type> 
class elementDrzewa
{
    public:
    type wartosc;
    elementDrzewa<type>* ojciec;
    Lista<type>* synowie;
    elementDrzewa<type>(type wart)
    {
        synowie = new Lista<type>();
        wartosc = wart;
    }
};
template <class type> 
class Drzewo
{
    public:
    elementDrzewa<type>* szef;
    Drzewo(type wart)
    {
        szef = new elementDrzewa<type>(wart);
        szef->ojciec = 0;
    }
    void dodaj(type wart, elementDrzewa<type>* ojciec);
    void usun(elementDrzewa<type>* elem);
    void preOrder(elementDrzewa<type>* wsk);
    void postOrder(elementDrzewa<type>* wsk);
    elementDrzewa<type>* zwrocKorzen()
    {
        return szef;
    }
    int zwrocWysokosc(elementDrzewa<type>* wezel);
};


template <class type> 
void Drzewo<type>::preOrder(elementDrzewa<type>* wsk)
{
	cout<<wsk->wartosc<<endl;
	for (int i=0; i<wsk->synowie->rozmiar();i++)
	{
		preOrder(wsk->synowie->zwrocElement(i+1));
	}
}

 
 template <class type> 
 void Drzewo<type>::postOrder(elementDrzewa<type>* wsk)
 {
	for (int i=0; i<wsk->synowie->rozmiar();i++)
	{
		postOrder(wsk->synowie->zwrocElement(i+1));
	}
	cout<<wsk->wartosc<<endl;
 }

 
 
 
 template <class type> 
void Drzewo<type>::usun(elementDrzewa<type>* elem)
{
    if(elem->synowie->rozmiar() != 0)
        cout << "Nie mozna usunac - wezel nie jest lisciem" << endl;
    else
    {
        elem->ojciec->synowie->usunElem(elem);
    }
}
template <class type> 
int Drzewo<type>::zwrocWysokosc(elementDrzewa<type>* wezel)
{
    int h = 0;
    if(wezel->synowie->rozmiar()==0)
        return 0;
    else
    {
        for(int i=1;i<=wezel->synowie->rozmiar();i++)
            h = max(h, zwrocWysokosc(wezel->synowie->zwrocElement(i)));
    }

    return 1+h;
}
template <class type> 
void Drzewo<type>::dodaj(type wart, elementDrzewa<type>* tata)
{

    elementDrzewa<type>* nowy = new elementDrzewa<type>(wart);
    tata->synowie->dodajElem(nowy);
    nowy->ojciec = tata;

}

int main()
{
    cout << "Tworze drzewo i ustawiam korzen drzewa na wartosc 0" << endl;
    Drzewo<int> drzewo(0);
    
    cout << "Wartosc korzenia: " << drzewo.zwrocKorzen()->wartosc << endl << endl;
    cout << "Wysokosc drzewa: " << drzewo.zwrocWysokosc(drzewo.szef) << endl;

    cout << "Dodaje dwoch synow do korzenia o wartosciach 5 i 10" << endl;
    drzewo.dodaj(5, drzewo.szef);
    drzewo.dodaj(10, drzewo.szef);
    cout << "1. Syn: " << drzewo.szef->synowie->zwrocElement(1)->wartosc << endl;
    cout << "2. Syn: " << drzewo.szef->synowie->zwrocElement(2)->wartosc << endl << endl;

    cout << "Dodaje dwoch synow do Syna 1 o wartosciach 7 i 9" << endl;
    drzewo.dodaj(7, drzewo.szef->synowie->zwrocElement(1));
    drzewo.dodaj(9, drzewo.szef->synowie->zwrocElement(1));
    cout << "1. Syn: " << drzewo.szef->synowie->zwrocElement(1)->wartosc << endl;
    cout << "1.1 Syn: " << drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(1)->wartosc << endl;
    cout << "1.2 Syn: " << drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2)->wartosc << endl;
    cout << "Wysokosc drzewa: " << drzewo.zwrocWysokosc(drzewo.szef) << endl << endl;

    cout << "Dodaje 3 synow do Syna 1.2 o wartosciach 11 15 20" << endl;
    drzewo.dodaj(11, drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2));
    drzewo.dodaj(15, drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2));
    drzewo.dodaj(20, drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2));
    cout << "1.2 Syn: " << drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2)->wartosc << endl;
    cout << "1.2.1 Syn: " << drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2)->synowie->zwrocElement(1)->wartosc << endl;
    cout << "1.2.2 Syn: " << drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2)->synowie->zwrocElement(2)->wartosc << endl;
    cout << "1.2.3 Syn: " << drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2)->synowie->zwrocElement(3)->wartosc << endl;
    cout << "Wysokosc drzewa: " << drzewo.zwrocWysokosc(drzewo.szef) << endl << endl;

    /*cout << "Usuwam synow 2  1.1  1.2.3" << endl;
    drzewo.usun(drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(2)->synowie->zwrocElement(3));
    drzewo.usun(drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(1));
    drzewo.usun(drzewo.szef->synowie->zwrocElement(2)); */
cout<<"xD"<<endl;
    cout << "Korzen ma: " << drzewo.szef->synowie->rozmiar() << " synow" << endl;
    cout << "Syn 1 ma: " << drzewo.szef->synowie->zwrocElement(1)->synowie->rozmiar() << " synow" << endl;
    cout << "Syn 1.2 ma: " << drzewo.szef->synowie->zwrocElement(1)->synowie->zwrocElement(1)->synowie->rozmiar() << " synow" << endl << endl;
cout<<"xD"<<endl;
    cout << "Przejscie pre-order: " << endl;
    drzewo.preOrder(drzewo.szef);
    cout << "Przejscie post-order: " << endl;
    drzewo.postOrder(drzewo.szef);

    return 0;
}

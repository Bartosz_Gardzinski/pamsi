#include <iostream>
#include <cstdlib>
#include <algorithm>




using namespace std;
template <class type> 
class EDrzewa
{
    public:
    type wartosc;
    EDrzewa<type>* ojciec;
    EDrzewa<type>* Lsyn;
    EDrzewa<type>* Psyn;
    EDrzewa<type>(type wart)
    {
        Lsyn=NULL;
        Psyn=NULL;
        wartosc = wart;
    }
};

template <class type> 
class Drzewo
{
    public:
    EDrzewa<type>* szef;
    Drzewo<type>(type wart)
    {
        szef = new EDrzewa<type>(wart);
        szef->ojciec = NULL;
    }
    void dodaj(type wart, EDrzewa<type>* ojciec);
    void usun(EDrzewa<type>* elem);
    void inOrder(EDrzewa<type>* w);
    EDrzewa<type>* zwrocKorzen()
    {
        return szef;
    }
    int zwrocWysokosc(EDrzewa<type>* wezel);
};

template <class type> 
void Drzewo<type>::dodaj(type wart, EDrzewa<type>* ojciec)
{
	EDrzewa<type>* nowy = new EDrzewa<type>(wart);
	if (ojciec->Lsyn==NULL)
	{
		ojciec->Lsyn=nowy;
		nowy->ojciec=ojciec;
	}
	else if (ojciec->Psyn==NULL)
	{
		ojciec->Psyn=nowy;
		nowy->ojciec=ojciec;
	}
	else
	{
		cout<<"Nie mozna dodac nowego elementu(wezel pelny)"<<endl;
		delete nowy;
	}
	
}

template <class type> 
void Drzewo<type>::usun(EDrzewa<type>* elem)
{
	if (elem->Lsyn!=NULL || elem->Psyn!=NULL)
		cout<<"Wezel ma synow, nie mozna usunac"<<endl;
	else if (elem->ojciec->Lsyn==elem)
	{
		elem->ojciec->Lsyn=NULL;
		delete elem;
	}
	else if (elem->ojciec->Psyn==elem)
	{
		elem->ojciec->Psyn=NULL;
		delete elem;
	}
}


template <class type> 
int Drzewo<type>::zwrocWysokosc(EDrzewa<type>* wezel)
{
    int h = 0;
    if(wezel->Psyn==NULL && wezel->Lsyn==NULL)
        return 0;
    else
    {
      
        if (wezel->Psyn!=NULL)
            h = max(h, zwrocWysokosc(wezel->Psyn));
        if (wezel->Lsyn!=NULL)
        	h = max(h, zwrocWysokosc(wezel->Lsyn));
    }

    return 1+h;
}



template <class type> 
void Drzewo<type>::inOrder(EDrzewa<type>* w)
{
	if (w->Lsyn!=NULL)
	{
		inOrder(w->Lsyn);
	}
	cout<<w->wartosc<<endl;
	if (w->Psyn!=NULL)
	{
		inOrder(w->Psyn);
	}
}







int main()
{
	Drzewo<int> buk(0);
	buk.dodaj(1,buk.szef);
	buk.dodaj(2,buk.szef);
	buk.dodaj(3,buk.szef->Psyn);
	buk.dodaj(4,buk.szef->Psyn);
	buk.dodaj(5,buk.szef->Psyn->Psyn);
	buk.dodaj(6,buk.szef->Psyn->Psyn->Lsyn);
	buk.dodaj(7,buk.szef->Psyn->Psyn->Lsyn);

	cout<<buk.szef->Psyn->Lsyn->wartosc<<endl;
	cout<<buk.szef->Psyn->Psyn->wartosc<<endl;
	
	cout<<buk.zwrocWysokosc(buk.szef)<<endl<<endl;
	
	buk.inOrder(buk.szef);
	
	return 0;
}
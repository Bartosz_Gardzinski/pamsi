
#include <algorithm>
#include <iostream>
using namespace std;

int main()
{
    int foo = 42, bar = 74;
    
    cout << "foo: " << foo << ", bar: " << bar << '\n';
    
    std::swap( foo, bar );
    
    cout << "foo: " << foo << ", bar: " << bar << '\n';
}
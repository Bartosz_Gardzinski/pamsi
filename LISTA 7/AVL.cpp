#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <ctime>
#include <windows.h>



using namespace std;

void info()
{
cout<<endl<<"MENU :"<<endl;
cout<<"1. Dodaj element "<<endl;
cout<<"2. Usun element "<<endl;
cout<<"3. Wyswietl "<<endl;
cout<<"4. Koniec programu "<<endl;
}


template <class type> 
class EDrzewa
{
    public:
    type wartosc;
    int bf;
    EDrzewa<type>* ojciec;
    EDrzewa<type>* Lsyn;
    EDrzewa<type>* Psyn;
    EDrzewa<type>(type wart)
    {
        Lsyn=NULL;
        Psyn=NULL;
        wartosc = wart;
    }
};

template <class type> 
class Drzewo
{
    public:
    EDrzewa<type>* szef;
    Drzewo<type>(type wart)
    {
        szef = new EDrzewa<type>(wart);
        szef->ojciec = NULL;
    }
    void dodaj(type wart, EDrzewa<type>* ojciec);
    void usun(EDrzewa<type>* elem);
    void inOrder(EDrzewa<type>* w);
    EDrzewa<type>* zwrocKorzen()
    {
        return szef;
    }
    int zwrocWysokosc(EDrzewa<type>* wezel);
};

template <class type> 
void Drzewo<type>::dodaj(type wart, EDrzewa<type>* ojciec)
{
	EDrzewa<type>* nowy = new EDrzewa<type>(wart);
	if (ojciec->Lsyn==NULL)
	{
		ojciec->Lsyn=nowy;
		nowy->ojciec=ojciec;
	}
	else if (ojciec->Psyn==NULL)
	{
		ojciec->Psyn=nowy;
		nowy->ojciec=ojciec;
	}
	else
	{
		cout<<"Nie mozna dodac nowego elementu(wezel pelny)"<<endl;
		delete nowy;
	}
	
}

template <class type> 
void Drzewo<type>::usun(EDrzewa<type>* elem)
{
	if (elem->Lsyn!=NULL || elem->Psyn!=NULL)
		cout<<"Wezel ma synow, nie mozna usunac"<<endl;
	else if (elem->ojciec->Lsyn==elem)
	{
		elem->ojciec->Lsyn=NULL;
		delete elem;
	}
	else if (elem->ojciec->Psyn==elem)
	{
		elem->ojciec->Psyn=NULL;
		delete elem;
	}
}


template <class type> 
int zwrocWysokosc(EDrzewa<type>* wezel)
{
    int h = 0;
    if(wezel->Psyn==NULL && wezel->Lsyn==NULL)
        return 0;
    else
    {
      
        if (wezel->Psyn!=NULL)
            h = max(h, zwrocWysokosc(wezel->Psyn));
        if (wezel->Lsyn!=NULL)
        	h = max(h, zwrocWysokosc(wezel->Lsyn));
    }

    return 1+h;
}



template <class type> 
void Drzewo<type>::inOrder(EDrzewa<type>* w)
{
	if (w->Lsyn!=NULL)
	{
		inOrder(w->Lsyn);
	}
	cout<<w->wartosc<<endl;
	if (w->Psyn!=NULL)
	{
		inOrder(w->Psyn);
	}
}





















template <class type> 
void RR(EDrzewa<type>*  & szef, EDrzewa<type> * A)
{
  EDrzewa<type> * B = A->Psyn;
  EDrzewa<type> * p = A->ojciec;

  A->Psyn = B->Lsyn;
  if(A->Psyn)
   A->Psyn->ojciec = A;

  B->Lsyn = A;
  B->ojciec = p;
  A->ojciec = B;

  if(p)
  {
    if(p->Lsyn == A)
	 p->Lsyn = B; 
	else p->Psyn = B;
  }
  else szef = B;

  if(B->bf == -1)
   A->bf = B->bf = 0;
  else
  {
    A->bf = -1; B->bf = 1;
  }
}


template <class type> 
void LL(EDrzewa<type> * & szef, EDrzewa<type> * A)
{
  EDrzewa<type> * B = A->Lsyn;
  EDrzewa<type> * p = A->ojciec;

  A->Lsyn = B->Psyn;
  if(A->Lsyn) A->Lsyn->ojciec = A;

  B->Psyn = A;
  B->ojciec = p;
  A->ojciec = B;

  if(p)
  {
    if(p->Lsyn == A) p->Lsyn = B; else p->Psyn = B;
  }
  else szef = B;

  if(B->bf == 1) A->bf = B->bf = 0;
  else
  {
    A->bf = 1; B->bf = -1;
  }
}


template <class type> 
void RL(EDrzewa<type> * & szef, EDrzewa<type> * A)
{
  EDrzewa<type> * B = A->Psyn;
  EDrzewa<type> * C = B->Lsyn;
  EDrzewa<type> * p  = A->ojciec;

  B->Lsyn = C->Psyn;
  if(B->Lsyn) B->Lsyn->ojciec = B;

  A->Psyn = C->Lsyn;
  if(A->Psyn) A->Psyn->ojciec = A;

  C->Lsyn = A;
  C->Psyn = B;
  A->ojciec = B->ojciec = C;
  C->ojciec = p;

  if(p)
  {
    if(p->Lsyn == A) p->Lsyn = C; else p->Psyn = C;
  }
  else szef = C;

  if(C->bf == -1) A->bf =  1; else A->bf = 0;
  if(C->bf ==  1) B->bf = -1; else B->bf = 0;

  C->bf = 0;
}

template <class type> 
void LR(EDrzewa<type> * & szef, EDrzewa<type> * A)
{
  EDrzewa<type> * B = A->Lsyn;
  EDrzewa<type> * C = B->Psyn;
  EDrzewa<type> * p = A->ojciec;

  B->Psyn = C->Lsyn;
  if(B->Psyn) B->Psyn->ojciec = B;

  A->Lsyn = C->Psyn;
  if(A->Lsyn) A->Lsyn->ojciec = A;

  C->Psyn = A;
  C->Lsyn = B;
  A->ojciec = B->ojciec = C;
  C->ojciec = p;

  if(p)
  {
    if(p->Lsyn == A) p->Lsyn = C; else p->Psyn = C;
  }
  else szef = C;

  if(C->bf ==  1) A->bf = -1; else A->bf = 0;
  if(C->bf == -1) B->bf =  1; else B->bf = 0;

  C->bf = 0;
}




































template <class type> 
void DodawanieAVL(EDrzewa<type> * & szef, type wart)
{
		EDrzewa<type> *w;
		EDrzewa<type> *temp1;
		EDrzewa<type> *temp2;
		bool t;
		
		w = new EDrzewa<type>(wart);
		w->wartosc=wart;
		w->bf=0;
		
		// DODAWANIE BST
		
		temp1=szef;
	
		
		
		if(!temp1) szef = w;       
 	 	     else
  		  		  { 
		
		while (1)
		{
		
			if (wart < temp1->wartosc)
			{
				
				if(!temp1->Lsyn)
				{
					temp1->Lsyn=w;
					break;
				}
				temp1=temp1->Lsyn;	
			}
		
		else
			
			{
					
				if(!temp1->Psyn)
				{
					temp1->Psyn=w;
					break;
				}	
				temp1=temp1->Psyn;	
			}
				
		}
				
		w->ojciec=temp1;
		
		//R�WNOWAGA DRZEWA AVL
if(temp1->bf) temp1->bf = 0; 
    else
    {
      if(temp1->Lsyn == w)   
        temp1->bf = 1;
      else
        temp1->bf = -1;

      temp2 = temp1->ojciec;        
                      
      t = false;
      while(temp2)
      {
        if(temp2->bf)
        {
          t = true;    
          break;        
        };
                        
        if(temp2->Lsyn == temp1) temp2->bf =  1;
        else             temp2->bf = -1;

        temp1 = temp2;          
        temp2 = temp2->ojciec;
      }

      if(t)             
      {                
        if(temp2->bf == 1)
        {
          if(temp2->Psyn == temp1) temp2->bf = 0;  
          else if(temp1->bf == -1) LR(szef,temp2);
          else                 LL(szef,temp2);
        }
        else
        {             
          if(temp2->Lsyn == temp1) temp2->bf = 0;  
          else if(temp1->bf == 1) RL(szef,temp2);
          else                RR(szef,temp2);
        }
      }
    }
  }
}

		

template <class type> 
EDrzewa<type> * pop(EDrzewa<type> * v)
{
  EDrzewa<type> * r;

  if(v)
  {
    if(v->Lsyn)
    {
      v = v->Lsyn;
      while(v->Psyn) v = v->Psyn;
    }
    else
      do
      {
        r = v;
        v = v->ojciec;
      } while(v && v->Psyn != r);
  }
  return v;
}



template <class type> 
EDrzewa<type> * findAVL(EDrzewa<type>* v, int k)
{
  while(v && v->wartosc != k)
    v = (k < v->wartosc) ? v->Lsyn: v->Psyn;

  return v;
}









template <class type> 
EDrzewa<type> * removeAVL(EDrzewa<type> * & szef, EDrzewa<type> * x)
{
  EDrzewa<type>  *t,*y,*z;
  bool nest;

  if(x->Lsyn && x->Psyn)
  {
    y    = removeAVL(szef,pop(x));
    nest = false;
  }
  else
  {
    if(x->Lsyn)
    {
      y = x->Lsyn; x->Lsyn = NULL;
    }
    else
    {
      y = x->Psyn; x->Psyn = NULL;
    }
    x->bf = 0;
    nest  = true;
  }

  if(y)
  {
    y->ojciec    = x->ojciec;
    y->Lsyn  = x->Lsyn;  if(y->Lsyn)  y->Lsyn->ojciec  = y;
    y->Psyn = x->Psyn; if(y->Psyn)  y->Psyn->ojciec = y;
    y->bf    = x->bf;
  }

  if(x->ojciec)
  {
    if(x->ojciec->Lsyn == x) x->ojciec->Lsyn = y; else x->ojciec->Psyn = y;
  }
  else szef = y;

  if(nest)
  {
    z = y;
    y = x->ojciec;
    while(y)
    {
      if(!y->bf)
      {              
        if(y->Lsyn == z)  y->bf = -1; else y->bf = 1;
        break;
      }
      else
      {
        if(((y->bf == 1) && (y->Lsyn == z)) || ((y->bf == -1) && (y->Psyn == z)))
        {          
          y->bf = 0;
          z = y; y = y->ojciec;
        }
        else
        {
          if(y->Lsyn == z)  t = y->Psyn; else t = y->Lsyn;
          if(!t->bf)
          {         
            if(y->bf == 1) LL(szef,y); else RR(szef,y);
            break;
          }
          else if(y->bf == t->bf)
          {         
            if(y->bf == 1) LL(szef,y); else RR(szef,y);
            z = t; y = t->ojciec;
          }
          else
          {         
            if(y->bf == 1) LR(szef,y); else RL(szef,y);
            z = y->ojciec; y = z->ojciec;
          }
        }
      }
    }
  }
  return x;
}




template <class type> 
void wypisz(EDrzewa<type>* wsk)
{
for(int j=0;j<depth(wsk);j++){
cout<<" ";
}
cout<<"L";
cout<<wsk->wartosc<<endl;
	
	
		if (wsk->Lsyn!=NULL)
	{
		wypisz(wsk->Lsyn);
		
	}
	if (wsk->Psyn!=NULL)
	{
		wypisz(wsk->Psyn);
	}
	
				
		
}
////////////////////////////////////////////////////////////////////////////////
template <class type> 
int depth(EDrzewa<type>* wsk)
{
	if(wsk->ojciec==NULL){
		return 0;}
		
		return(1+ depth(wsk->ojciec));
}











int main()
{
	int i; EDrzewa<int> * root = NULL;	for(i = 0; i < 32; i++)   { DodawanieAVL(root,1);}

	
	
	
	
	
	
	
	
	
	
	
 EDrzewa<int> * szef = NULL;
int a;
cout<<"Podaj wartosc roota"<<endl;
 cin>>a;
DodawanieAVL(szef,a);
              
 int s;
 while(true)
	{
	system("CLS");	
	
	info();
		cin>>s;
		switch(s)
		{
			case 1:
				cout<<"Podaj wartosc elementu, ktory chcesz dodac"<<endl;
				cin>>a;
                DodawanieAVL(szef,a);
				break;

            case 2:
            	cout<<"Podaj wartosc elementu, ktory chcesz usunac"<<endl;
				cin>>a;
                removeAVL(szef,findAVL(szef,a));
                system("pause");
				break;
			
			case 3:
			    wypisz(szef);
				system("pause");
			    break;		
			
			case 4:
			             exit(0);
				break;
			
            default:

                cout<<"nie ma takiej opcji "<<endl;
                system("pause");
				break;
		}
	}


  
  
	return 0;
}